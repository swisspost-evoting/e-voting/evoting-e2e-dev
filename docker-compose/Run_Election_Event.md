# Run an Election Event in a local Docker Development Environment

**The Secure Data Manager must be installed on a Windows machine. An installation on Linux or macOS is currently not supported.**\
**In order to facilitate the testing in a local development environment this guide simplifies certain configurations.**\
**All keys, certificates and passwords provided are pre-generated for the purpose of these tests only.**\
**In real election events the transfers between the air-gapped machines are performed with USB Sticks.**\
**Make sure the system is ready using the ```docker ps``` command to check that all docker containers are healthy.**

**Placeholders:**

- SEED: Seed folder name (e.g. `_EE_20231124_TT05`)

## Pre-requisites

Read the main [README](../README.md) file to understand the prerequisites and the folder structure.

## Computers

Running an Election Event requires at least four separate computers. To facilitate testing, this guide describes how to run a test when all four
computers are simulated on a single machine:

- ```C:/tmp/computers/PC-ONLINE```
- ```C:/tmp/computers/PC-SETUP```
- ```C:/tmp/computers/PC-TALLY```
- ```C:/tmp/computers/PC-VERIFIER```

### Data Integration Service

The Data Integration Service consumes the eCH files and generates the configuration files for the Election Event. It is installed inside
the `PC-SETUP`.

### Verifier

The Verifier verifies the Election Event data. It is installed inside the `PC-VERIFIER`.

### Secure Data Manager (SDM)

#### One instance of the SDM is deployed on a computer connected to the internet:

- **Online SDM**: Connects to the voter-portal. It is installed on the `PC-ONLINE`.

#### Two instances of the SDM are deployed on airgapped computers:

- **Setup SDM**: Generates the Election Event configuration. It is installed on the `PC-SETUP`.
- **Tally SDM**: Performs the final mixing and decrypting and processes the decrypted votes. It is installed on the `PC-TALLY`.

### Shortcuts

Upon the following sentences execute the given action:

- Launch the Setup SDM:
    - Click on the shortcut file `PC-SETUP - Secure Data Manager` located at: `C:/tmp/computers/`.
        - In case you did not run the `e2e.sh` script as an administrator, the shortcut does not exist. Therefore, click on the
          file `SecureDataManager.exe` located at: `C:/tmp/computers/PC-SETUP/secure-data-manager`.
    - Select the only existing election event (e.g. `NE_20231124_TT05 [SETUP]`) and click on `Select`.
    - Click on `Start SDM` (checking the configuration is not needed as it has been set for you).
    - Wait until the UI is completely loaded. The Setup SDM has a *red* header.
- Launch the Online SDM:
    - Click on the shortcut file `PC-ONLINE - Secure Data Manager` located at: `C:/tmp/computers/`.
        - In case you did not run the script `e2e.sh` as an administrator, the shortcut does not exist. Therefore, click on the
          file `SecureDataManager.exe` located at: `C:/tmp/computers/PC-ONLINE/secure-data-manager`.
    - Select the only existing election event (e.g. `NE_20231124_TT05 [ONLINE]`) and click on `Select`.
    - Click on `Start SDM` (checking the configuration is not needed as it has been set for you).
    - Wait until the UI is completely loaded. The Online SDM has a *blue* header.
    - Check that there is a stable connection to the Voting Server (green mark on top right of the UI).
- Launch the Tally SDM:
    - Click on the shortcut file `PC-TALLY - Secure Data Manager` located at: `C:/tmp/computers/`.
        - In case you did not run the script `e2e.sh` as an administrator, the shortcut does not exist. Therefore, click on the
          file `SecureDataManager.exe` located at: `C:/tmp/computers/PC-TALLY/secure-data-manager`.
    - Select the only existing election event (e.g. `NE_20231124_TT05 [TALLY]`) and click on `Select`.
    - Click on `Start SDM` (checking the configuration is not needed as it has been set for you).
    - Wait until the UI is completely loaded. The Tally SDM has a *green* header.
- Launch the Verifier:
    - Click on the shortcut file `PC-VERIFIER - Verifier` located at: `C:/tmp/computers/`.
        - In case you did not run the script `e2e.sh` as an administrator, the shortcut does not exist. Therefore, click on the
          file `Verifier.exe` located at: `C:/tmp/computers/PC-VERIFIER/verifier`.
    - Wait until the UI is completely loaded.

---

## Day 1 - Setting up the election event

### Data Integration Service - Process input eCH files.

**Workflow**

1. Run the Data Integration Service by executing the shortcut file `PC-SETUP - Data Integration Service` located at: `C:/tmp/computers/`.
    - In case you did not run the script `e2e.sh` as an administrator, the shortcut does not exist. Therefore, click on the
      file `run-data-integration-service.bat` located at: `C:/tmp/computers/PC-SETUP/data-integration-service`.
2. Once executed, two files will be created in `C:/tmp/computers/PC-SETUP/<SEED>/DIS-output`:
    - `configuration.xml`: This file is *not* used in the next steps.
    - `configuration-anonymized.xml`: This file is used in the next steps.

### Setup SDM - Preconfiguration of the event

**Workflow**

1. Launch the Setup SDM.
2. The UI displays the details of the E2E election event. Check the box **I confirm that all the information is correct.** at the bottom of the page.
3. Click on **Preconfigure**, wait for the process to finish successfully and click on **Next**.
4. Click on **Precompute**, wait for the process to finish successfully and click on **Next**.
5. Click on **Export** and wait for the process to finish successfully.
6. Close the Setup SDM.

### Online SDM - Encryption Keys request and Voting Cards computation

**Workflow:**

1. Launch the Online SDM.
2. Click on **Import** and select the previously exported file (`export-[1]-*.sdm`) in `C:/tmp/computers/PC-SETUP/<SEED>/SDM-Output`.
3. Wait for the process to finish successfully and click on **Next**.
4. Click on **Request**, wait for the process to finish successfully and then click on **Next**.
5. Click on **Compute**, wait for the process to finish successfully and then click on **Next**.
6. Click on **Download**, wait for the process to finish successfully and then click on **Next**.
7. Click on **Export** and wait for the process to finish successfully.
8. Close the Online SDM.

### Setup SDM - Generation of Voting Cards and print file

**Workflow:**

1. Launch the Setup SDM.
2. Click on **Import** and select the previously exported file (`export-[2]-*.sdm`) in `C:/tmp/computers/PC-ONLINE/<SEED>/SDM-Output`.
3. Wait for the process to finish successfully and click on **Next**.
4. Click on **Generate**, wait for the process to finish successfully and then click on **Next**.
5. Click on **Generate**, wait for the process to finish successfully and then click on **Next**.
6. Click on **Export** and wait for the process to finish successfully.
7. Close the Setup SDM.

### Online SDM - Upload at the end of Day 1

1. Launch the Online SDM.
2. Click on **Import** and select the previously exported file (`export-[3]-*.sdm`) in `C:/tmp/computers/PC-SETUP/<SEED>/SDM-Output`.
3. Wait for the process to finish successfully and click on **Next**.
4. Click on **Upload** and wait for the process to finish successfully.
5. Close the Online SDM.

---

## Day 2 - Release of the election event

### Setup SDM - Constitution of the Electoral Board and Verifier data collection

**Workflow:**

1. Launch the Setup SDM.
2. Choose and confirm the password for the first Electoral Board member:
    + We suggest the password `Password_EB1_Password_EB1`, for the purpose of this test.
    + Click on **Set Password**.
3. Choose and confirm the password for the second Electoral Board member:
    + We suggest the password `Password_EB2_Password_EB2`, for the purpose of this test.
    + Click on **Set Password**.
4. Click on **Constitute**, wait for the process to finish successfully and then click on **Next**.
5. Click on **Collect**, wait for the process to finish successfully and then click on **Next**.
6. Click on **Export** and wait for the process to finish successfully.
7. Close the Setup SDM.

### Verifier - Verification of the election event data

**Workflow:**

1. Launch the Verifier.
2. Select the `Setup Mode`
3. Click on **Upload Context Dataset** and select the file `Dataset-context-*.zip` in `C:/tmp/computers/PC-SETUP/<SEED>/Verifier-Output`.
4. Wait for the process to finish successfully.
5. Click on **Upload Setup Dataset** and select the file `Dataset-setup-*.zip` in `C:/tmp/computers/PC-SETUP/<SEED>/Verifier-Output`.
6. Wait for the process to finish successfully.
7. Click on **Verify** and wait for all verifications to be in `Success` tab.
8. Manually check that all information displayed in **print mode** is correct.
9. Close the Verifier.

### Online SDM - Upload at the end of Day 2

**Workflow:**

1. Launch the Online SDM.
2. Click on **Import** and select the previously exported file (`export-[4]-*.sdm`) in `C:/tmp/computers/PC-SETUP/<SEED>/SDM-Output`.
3. Wait for the process to finish successfully and click on **Next**.
4. Click on **Upload** and wait for the process to finish successfully.
5. Click on **Copy URL** to save the Voter Portal URL for the next steps and click on **Next**.
6. Close the Online SDM.

---

## Voting phase - Submit Votes from the browser

Voting is done from a WEB browser.

### Voting cards

To submit a vote, you need to enter the following voting card information:

+ Date of birth: `01.06.1944` (same for all voters of the provided test data).
+ Start Voting Key: `initialization code` (e.g. fxyugfs7x3gvnyg36poevv4e)
+ Ballot Casting Key: `confirmation code` (e.g. 558570817)
+ Vote Cast Return Code: `finalization code` (e.g. 50383416)

All voting cards are stored in ```C:/tmp/computers/PC-SETUP/<SEED>/Print-Output/evoting-print_*.xml```. Each voting card is stored in a
tag `<votingCard>...</votingCard>`. For each voting card, the last 3 pieces of information required to submit a vote can be found inside the tag:

```xml

<votingCard>
	...
	<startVotingKey>fxyugfs7x3gvnyg36poevv4e</startVotingKey> <!-- contains the initialization code -->
	<ballotCastingKey>558570817</ballotCastingKey> <!-- contains confirmation code -->
	<voteCastReturnCode>50383416</voteCastReturnCode> <!-- contains finalization code -->
	...
</votingCard>
```

**Workflow:**

1. Paste the previously copied URL in your browser: (e.g. `http://localhost:7000/vote/#/legal-terms/D1D66900C976B2738FE7883C9A29F816`).
2. Acknowledge the two **Legal Terms** and click on **Next**.
3. In order to authenticate yourself as a voter, enter:
    + the `initialization code`.
    + the date of birth (`01.06.1944`).
4. Click on **Start**.
5. Make your selection and click on **Next**.
6. Review your selection, click on **Seal ballot**, click on **Yes, seal vote** and wait for the process to finish successfully.
7. Confirm your selection and enter your `confirmation code`.
8. Click on **Cast vote** and wait for the process to finish successfully.
9. Verify that the displayed `finalization code` matches yours.
10. Click on **Quit**.
11. Repeat the process for another voting card or close the browser when you want to proceed.

---

## Day 3 - Closing of the election event

### Online SDM - Mixing and Downloading Ballot Box(es)

**Workflow:**

1. Launch the Online SDM.
2. Select all the ballot boxes using the table checkbox.
3. Click **Mix and Download Ballot Boxes** to mix and download the ballot boxes.
4. Click **Confirm** to start the mixing and downloading process and wait for the process to finish successfully.
5. Click **Export**, wait for the process to finish successfully and click on **Close**.
6. Close the Online SDM.

---

### Tally SDM - Decryption of the Ballot Boxes and Verifier data collection

**Workflow:**

1. Launch the Tally SDM.
2. Click on **Import** and select the previously exported file (`export-[5]-*.sdm`) in `C:/tmp/computers/PC-ONLINE/<SEED>/SDM-Output`.
3. Wait for the process to finish successfully and click on **Next**.
4. Select all the ballot boxes using the table checkbox.
5. Click on **Decrypt Ballot Boxes**.
6. Enter the password for the first Electoral Board member and click on **Validate**.
    + The suggested password was `Password_EB1_Password_EB1`, for the purpose of this test.
7. Enter the password for the second Electoral Board member and click on **Validate**.
    + The suggested password was `Password_EB2_Password_EB2`, for the purpose of this test.
8. Click on **Decrypt**, wait for the process to finish successfully and click on **Next**.
9. Click **Collect** and wait for the process to finish successfully.
10. Close the Tally SDM.

### Verifier - Verification of the decrypted election event data

**Workflow:**

1. Launch the Verifier.
2. Select the `Tally Mode`
3. Click on **Upload Context Dataset** and select the file `Dataset-context-*.zip` in `C:/tmp/computers/PC-SETUP/<SEED>/Verifier-Output`.
4. Wait for the process to finish successfully.
5. Click on **Upload Tally Dataset** and select the file `Dataset-tally-*.zip` in `C:/tmp/computers/PC-TALLY/<SEED>/Verifier-Output`.
6. Wait for the process to finish successfully.
7. Click on **Verify** and wait for all verifications to be in `Success` tab.
8. Manually check that all information displayed in **print mode** is correct.
9. Close the Verifier.

---

## Data deletion

To re-run an Election Event a cleanup of the previous Election Event is needed.

**Workflow:**

1. Execute the command `C:/tmp/evoting-e2e-dev/scripts/prepare-computers-e2e.sh --path C:/tmp/computers` in an administrator command prompt otherwise
   the script will not be able to create the shortcuts.
2. When prompted for the deletion of `C:/tmp/computers`, answer `y` to confirm the deletion.
3. Go to Day 1 and start the process again.
