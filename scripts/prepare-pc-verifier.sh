#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#
#

# Exit if any line failed
set -e

DIRECT_TRUST="direct-trust"
VERIFIER="verifier"
WORKSPACE="workspace"
PC_NAME="PC-VERIFIER"

DIR=''
VERIFIER_PACKAGE=''
DECRYPTION_PASSWORD=''
DIRECT_TRUST_KEYSTORE=''
DIRECT_TRUST_PASSWORD=''
CREATE_SHORTCUTS=false

source "${EVOTING_INFRASTRUCTURE_HOME}"/scripts/utils.sh

check_mandatory_options() {
  if [ -z "$DIR" ]; then
      error "--path [DIRECTORY] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$VERIFIER_PACKAGE" ]; then
      error "--verifier [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$DECRYPTION_PASSWORD" ]; then
      error "--decryption-password [STRING] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$DIRECT_TRUST_KEYSTORE" ]; then
      error "--direct-trust-keystore [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$DIRECT_TRUST_PASSWORD" ]; then
      error "--direct-trust-password [FILE] option is expected. See --help for more information."
      exit 1
  fi
}

create_verifier_shortcut() {
  if [ "$CREATE_SHORTCUTS" = true ] ; then
    # Check if running on Windows
    if [[ "$(uname -s)" == "MINGW"* ]]; then
      # Check if running with administrative privileges
      if net session > /dev/null 2>&1; then
        info "Creating Verifier shortcut..."

        # Create the symbolic link
        SYMBOLIC_LINK="$DIR/$PC_NAME - Verifier"

        # Create the symbolic link using PowerShell
        powershell -Command "New-Item -ItemType SymbolicLink -Path \"$SYMBOLIC_LINK\" -Target \"$PC_VERIFIER_VERIFIER/Verifier.exe\"" > /dev/null 2>&1;

        info "You can now run $SYMBOLIC_LINK."
        return
      else
        warn "This script does not seem to be running with administrative privileges. Shortcut will NOT be created."
      fi
    else
      warn "This script does not seem to be running on Windows. Shortcut will NOT be created."
    fi
  fi
  info "You can now run $PC_VERIFIER_VERIFIER/Verifier.exe."
}

prepare_verifier() {

  # Validate the Verifier assembly is present
  if compgen -G "$VERIFIER_PACKAGE" > /dev/null; then
    info "Verifier assembly found with version $(basename $VERIFIER_PACKAGE | sed 's/verifier-assembly-\(.*\)\.zip/\1/')."
  else
    warn "Impossible to find the assembled Verifier. Please run the Verifier assembly to be able to install the Verifier in ${PC_NAME}. Ignoring."
    exit 0
  fi

  # Unzip verifier
  DIR_VERIFIER="$DIR/$VERIFIER"
  mkdir "$DIR_VERIFIER"

  info "Starting extraction of Verifier. This may take a while."
  unzip -o "$VERIFIER_PACKAGE" -d "$DIR_VERIFIER" > /dev/null

  info "Configuring Verifier..."

  PC_VERIFIER_WORKSPACE="$PC_VERIFIER/$WORKSPACE"
  mkdir -p "$PC_VERIFIER_WORKSPACE"

  PC_VERIFIER_DIRECT_TRUST="$PC_VERIFIER/$DIRECT_TRUST"
  mkdir -p "$PC_VERIFIER_DIRECT_TRUST"
  cp "$DIRECT_TRUST_KEYSTORE" "$PC_VERIFIER_DIRECT_TRUST"
  cp "$DIRECT_TRUST_PASSWORD" "$PC_VERIFIER_DIRECT_TRUST"

  PC_VERIFIER_VERIFIER="$PC_VERIFIER/$VERIFIER"
  cp -R "$DIR_VERIFIER" "$PC_VERIFIER"

  PC_VERIFIER_APPLICATION_PROPERTIES_FILE="$PC_VERIFIER_VERIFIER/resources/application.properties"
  sed -i "s|import\.zip\.decryption\.password=|import\.zip\.decryption\.password=$DECRYPTION_PASSWORD|g" "$PC_VERIFIER_APPLICATION_PROPERTIES_FILE"
  sed -i "s|^#dataset\.unzip\.location=$|dataset\.unzip\.location=${PC_VERIFIER_WORKSPACE}|g" "$PC_VERIFIER_APPLICATION_PROPERTIES_FILE"
  PC_VERIFIER_DIRECT_TRUST_PATH="$PC_VERIFIER_DIRECT_TRUST/local_direct_trust_keystore_verifier.p12"
  PC_VERIFIER_DIRECT_TRUST_PWD_PATH="$PC_VERIFIER_DIRECT_TRUST/local_direct_trust_pw_verifier.txt"
  sed -i "s|./direct-trust/public_keys_keystore_verifier\.p12|${PC_VERIFIER_DIRECT_TRUST_PATH}|g" "$PC_VERIFIER_APPLICATION_PROPERTIES_FILE"
  sed -i "s|./direct-trust/public_keys_keystore_verifier_pw\.txt|${PC_VERIFIER_DIRECT_TRUST_PWD_PATH}|g" "$PC_VERIFIER_APPLICATION_PROPERTIES_FILE"

  info "Removing originally extracted Verifier assembly..."
  rm -rf "$DIR_VERIFIER"

  create_verifier_shortcut
}

############
### Main ###
############

while [[ $# -gt 0 ]]; do
  case $1 in
    --path)
      DIR=$(cygpath.exe --mixed -a "$2")
      shift
      shift
      ;;
    --verifier)
      VERIFIER_PACKAGE="$2"
      shift
      shift
      ;;
    --decryption-password)
      DECRYPTION_PASSWORD="$2"
      shift
      shift
      ;;
    --direct-trust-keystore)
      DIRECT_TRUST_KEYSTORE="$2"
      shift
      shift
      ;;
    --direct-trust-password)
      DIRECT_TRUST_PASSWORD="$2"
      shift
      shift
      ;;
    --create-shortcuts)
      CREATE_SHORTCUTS=true
      shift
      ;;
    -h|--help)
      echo "Usage: prepare-pc-verifier.sh [OPTIONS]"
      echo "Prepare the verifier PC. The script will initiate the needed folders for executing:"
      echo -e '\t - the Verifier'
      echo
      echo "Options:"
      echo -e '--path [DIRECTORY] \t\t\t Provide the path to the target directory where the PC will be created.'
      echo -e '--verifier [FILE] \t\t\t Provide the path to the Verifier package file to be installed.'
      echo -e '--decryption-password [STRING] \t\t Provide the decryption password.'
      echo -e '--direct-trust-keystore [FILE] \t\t Provide the path to the direct trust keystore file to be used in the Verifier.'
      echo -e '--direct-trust-password [FILE] \t\t Provide the path to the direct trust password file to be used in the Verifier.'
      echo -e '--create-shortcuts \t\t Optional: create shortcuts for the applications in provided path directory.'
      echo
      exit 0
      ;;
    *)
      error "Unknown option $1"
      exit 1
      ;;
  esac
done

check_mandatory_options

PC_VERIFIER="$DIR/$PC_NAME"

# Prepare target directory (exist or create, is empty)
if [ -d "$DIR" ]
then
	if [ -d "$PC_VERIFIER" ]; then
    error "$PC_VERIFIER already exists. Consider removing it before retrying. Exiting."
    exit 1
	fi
else
  mkdir -p "$DIR"
	info "Directory $DIR created."
fi

############
### Main ###
############

info "Creating $PC_VERIFIER..."
mkdir "$PC_VERIFIER"

prepare_verifier
