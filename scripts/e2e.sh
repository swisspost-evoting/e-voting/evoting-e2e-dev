#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#

set -o errexit

archive_name="build.tar.gz"
e_voting="e-voting"
verifier="verifier"
data_integration_service="data-integration-service"
evoting_e2e_dev="evoting-e2e-dev"

# Define color codes
RED='\033[0;31m'
BLUE='\033[0;34m'
GREY='\033[0;90m'
RESET='\033[0m'  # Reset text formatting

info() {
    echo -e "${GREY}$(date +"%T")${RESET} [${BLUE}INFO${RESET}] $1"
}

error() {
    echo -e "${RED}$(date +"%T") [ERROR] $1${RESET}"
}

while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      echo "Usage: e2e.sh"
      echo "Starts the end-to-end environment of e-voting."
      echo "E-voting services will start as docker containers and the Secure Data Manager will be configured for an end-to-end test."
      echo
      echo "The script expects one of the following folder hierarchies to be true:"
      echo "1. <any directory>"
      echo "   |- ${archive_name} (This archive is generated by the evoting-build Docker image)"
      echo "   |- e2e.sh"
      echo
      echo "2. <any directory>"
      echo "   |- ${e_voting}/ (built)"
      echo "   |- ${verifier}/ (built)"
      echo "   |- ${data_integration_service}/ (built)"
      echo "   |- ${evoting_e2e_dev}/"
      echo "   |- e2e.sh"
      echo
      echo "WARNING: This script only works if docker and docker compose is installed on the machine running it."
      exit 0
      ;;
    *)
      error "Unknown option $1"
      exit 1
      ;;
  esac
done

if [[ ! -d "${e_voting}" ]] || [[ ! -d "${evoting_e2e_dev}" ]]; then
  if [ ! -f "${archive_name}" ]; then
    error "Missing ${archive_name}!"
    exit 1
  fi
  info "Opening ${archive_name} archive. Please wait..."
  tar -xf ${archive_name}
  info "Archive successfully opened."
else
  info "Folders ${e_voting} and ${evoting_e2e_dev} are already present."
fi

current_dir=$(pwd)

export EVOTING_INFRASTRUCTURE_HOME=$current_dir/${evoting_e2e_dev}
export DOCKER_REGISTRY=registry.gitlab.com/swisspost-evoting/e-voting/${evoting_e2e_dev}/
export DATA_INTEGRATION_SERVICE_HOME=$current_dir/${data_integration_service}
export EVOTING_HOME=$current_dir/${e_voting}
export VERIFIER_HOME=$current_dir/${verifier}

$EVOTING_INFRASTRUCTURE_HOME/scripts/prepare-e2e.sh
