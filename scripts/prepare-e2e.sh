#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#

# Script to setup the docker dev environment for an end to end.
#

set -o errexit

source "$EVOTING_INFRASTRUCTURE_HOME"/scripts/copy-evoting-version-in-env-file.sh
source "${EVOTING_INFRASTRUCTURE_HOME}"/scripts/utils.sh

define_compose_file_options() {
  composeFileOptions="-f ${EVOTING_INFRASTRUCTURE_HOME}/docker-compose/docker-compose.common.yml -f ${EVOTING_INFRASTRUCTURE_HOME}/docker-compose/docker-compose.postgres.yml"
}

rebuild_service_images() {
  info "Rebuilding docker services images. Please wait..."
  docker compose ${composeFileOptions} build --quiet
}

##########################
########## Main ##########
##########################

info "Resetting the test-data. Please wait..."
reset_test_data
define_compose_file_options
rebuild_service_images

info "Stopping all services if running. Please wait..."
docker compose ${composeFileOptions} stop
info "Creating and starting all services. Please wait..."
docker compose ${composeFileOptions} up -d --force-recreate

COMPUTERS='computers'
info "Preparing $COMPUTERS folder. Please wait..."
${EVOTING_INFRASTRUCTURE_HOME}/scripts/prepare-computers-e2e.sh --path "${COMPUTERS}"

info "End-to-end environment of e-voting ready!"

info "Starting to listen on docker container logs..."
docker compose ${composeFileOptions} logs --follow | grep --colour "SEVERE\|ERROR\|WARN"
