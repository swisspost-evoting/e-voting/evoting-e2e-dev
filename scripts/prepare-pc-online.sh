#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#
#

# Exit if any line failed
set -e

# Global variables.
PC_NAME="PC-ONLINE"

# Secure Data Manager folders.
SDM="secure-data-manager"
SDM_WORKSPACE="SDM-workspace"
SDM_OUTPUT="SDM-Output"

DEBUG_CONFIG=''
DIR=''
SECURE_DATA_MANAGER_PACKAGE=''
SEED=''
SETTINGS_JSON=''
STOP_DOCKER=false
CREATE_SHORTCUTS=false
SKIP_UNZIP_SDM=false
SKIP_DELETE_UNZIP_SDM=false

source "${EVOTING_INFRASTRUCTURE_HOME}"/scripts/utils.sh

check_mandatory_options() {
  if [ -z "$DEBUG_CONFIG" ]; then
      error "--debug-config [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$DIR" ]; then
      error "--path [DIRECTORY] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SECURE_DATA_MANAGER_PACKAGE" ]; then
      error "--secure-data-manager [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SEED" ]; then
      error "--seed [STRING] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SETTINGS_JSON" ]; then
      error "--settings [FILE] option is expected. See --help for more information."
      exit 1
  fi
}

create_secure_data_manager_shortcut() {
  if [ "$CREATE_SHORTCUTS" = true ] ; then
    # Check if running on Windows
    if [[ "$(uname -s)" == "MINGW"* ]]; then
      # Check if running with administrative privileges
      if net session > /dev/null 2>&1; then
        info "Creating Secure Data Manager shortcut..."

        # Create the symbolic link
        SYMBOLIC_LINK="$DIR/$PC_NAME - Secure Data Manager"

        # Create the symbolic link using PowerShell
        powershell -Command "New-Item -ItemType SymbolicLink -Path \"$SYMBOLIC_LINK\" -Target \"$PC_ONLINE_SDM/SecureDataManager.exe\"" > /dev/null 2>&1;

        info "You can now run $SYMBOLIC_LINK."
        return
      else
        warn "This script does not seem to be running with administrative privileges. Shortcut will NOT be created."
      fi
    else
      warn "This script does not seem to be running on Windows. Shortcut will NOT be created."
    fi
  fi
  info "You can now run $PC_ONLINE_SDM/SecureDataManager.exe."
}

prepare_secure_data_manager() {

  # Unzip sdm
  DIR_SDM="$DIR/$SDM"
  if [ "$SKIP_UNZIP_SDM" = false ] ; then
    # Validate the Secure Data Manager package is present
    if compgen -G "$SECURE_DATA_MANAGER_PACKAGE" > /dev/null; then
      info "Secure Data Manager package found with version $(basename $SECURE_DATA_MANAGER_PACKAGE | sed 's/secure-data-manager-package-\(.*\)\.zip/\1/')."
    else
      warn "Impossible to find the packaged Secure Data Manager. Please run the Secure Data Manager packaging to be able to install the Secure Data Manager in ${PC_NAME}. Ignoring."
      exit 0
    fi

    mkdir "$DIR_SDM"
    info "Starting extraction of Secure Data Manager. This may take a while."
    unzip -o "$SECURE_DATA_MANAGER_PACKAGE" -d "$DIR_SDM" > /dev/null
  fi

  info "Configuring Secure Data Manager..."

  PC_ONLINE_WORKSPACE="$PC_ONLINE_SEED/$SDM_WORKSPACE"
  mkdir -p "$PC_ONLINE_WORKSPACE"

  PC_ONLINE_OUTPUT="$PC_ONLINE_SEED/$SDM_OUTPUT"
  mkdir -p "$PC_ONLINE_OUTPUT"

  PC_ONLINE_SDM="$PC_ONLINE/$SDM"
  cp -R "$DIR_SDM" "$PC_ONLINE"

  cp "$DEBUG_CONFIG" "$PC_ONLINE_SDM/resources/app/electron"
  cp "$SETTINGS_JSON" "$PC_ONLINE_SDM/resources/app/electron"

  PC_ONLINE_SETTINGS_FILE="$PC_ONLINE_SDM/resources/app/electron/settings.json"
  sed -i "s#\"value\": \"OUTPUT_PATH_TO_REPLACE\"#\"value\": \"$PC_ONLINE_OUTPUT\"#" "$PC_ONLINE_SETTINGS_FILE"
  sed -i "s#\"value\": \"ONLINE_WORKSPACE_TO_REPLACE\"#\"value\": \"$PC_ONLINE_WORKSPACE\"#" "$PC_ONLINE_SETTINGS_FILE"

  if [ "$SKIP_DELETE_UNZIP_SDM" = false ] ; then
    info "Removing originally extracted Secure Data Manager package..."
    rm -rf "$DIR_SDM"
  fi

  if [ "$STOP_DOCKER" = true ] ; then
    # Stop Secure Data Manager dockerized instance if exist.
    info "Stopping Docker Secure Data Manager online container..."
    docker stop secure-data-manager-online > /dev/null 2>&1 || true
  fi

  create_secure_data_manager_shortcut
}

############
### Main ###
############

while [[ $# -gt 0 ]]; do
  case $1 in
    --debug-config)
      DEBUG_CONFIG="$2"
      shift
      shift
      ;;
    --path)
      DIR=$(cygpath.exe --mixed -a "$2")
      shift
      shift
      ;;
    --secure-data-manager)
      SECURE_DATA_MANAGER_PACKAGE="$2"
      shift
      shift
      ;;
    --seed)
      SEED="$2"
      shift
      shift
      ;;
    --settings)
      SETTINGS_JSON="$2"
      shift
      shift
      ;;
    --stop-docker)
      STOP_DOCKER=true
      shift
      ;;
    --create-shortcuts)
      CREATE_SHORTCUTS=true
      shift
      ;;
    --skip-unzip-sdm)
      SKIP_UNZIP_SDM=true
      shift
      ;;
    --skip-delete-unzip-sdm)
      SKIP_DELETE_UNZIP_SDM=true
      shift
      ;;
    -h|--help)
      echo "Usage: prepare-pc-online.sh [OPTIONS]"
      echo "Prepare the online PC. The script will initiate the needed folders for executing:"
      echo -e '\t - the Secure Data Manager'
      echo
      echo "Options:"
      echo -e '--debug-config [FILE] \t\t Provide the path to the debug configuration file to be used in the Secure Data Manager.'
      echo -e '--path [DIRECTORY] \t\t Provide the path to the target directory where the PC will be created.'
      echo -e '--secure-data-manager [FILE] \t Provide the path to the Secure Data Manager package file to be installed.'
      echo -e '--seed [STRING] \t\t Provide the seed string.'
      echo -e '--settings [FILE] \t\t Provide the path to the settings file to be used in the Secure Data Manager.'
      echo -e '--stop-docker \t\t\t Optional: stop the Docker Secure Data Manager online container.'
      echo -e '--create-shortcuts \t\t Optional: create shortcuts for the applications in provided path directory.'
      echo -e '--skip-unzip-sdm \t\t Optional: skip the unzip of the Secure Data Manager package.'
      echo -e '--skip-delete-unzip-sdm \t Optional: skip the deletion of the unzipped Secure Data Manager package.'
      echo
      exit 0
      ;;
    *)
      error "Unknown option $1"
      exit 1
      ;;
  esac
done

check_mandatory_options

PC_ONLINE="$DIR/$PC_NAME"

# Prepare target directory (exist or create, is empty)
if [ -d "$DIR" ]
then
	if [ -d "$PC_ONLINE" ]; then
     error "$PC_ONLINE already exists. Consider removing it before retrying. Exiting."
     exit 1
	fi
else
  mkdir -p "$DIR"
	info "Directory $DIR created."
fi

info "Creating $PC_ONLINE..."
mkdir "$PC_ONLINE"

PC_ONLINE_SEED="$PC_ONLINE/$SEED"
mkdir "$PC_ONLINE_SEED"

prepare_secure_data_manager
