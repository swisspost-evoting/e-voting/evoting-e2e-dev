#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#
#

set -o errexit

DIR=''
DATASET='Post_E2E_DEV'

source "${EVOTING_INFRASTRUCTURE_HOME}"/scripts/utils.sh

check_mandatory_options() {

  if [ -z "$DIR" ]; then
      error "Either the --path [DIRECTORY] option is expected or the EVOTING_COMPUTERS_HOME environment variable is required. See --help for more information."
      exit 1
  fi

  if [ -z "$EVOTING_HOME" ]; then
      error "EVOTING_HOME environment variable is required. See --help for more information."
      exit 1
  fi

  if [ -z "$DATA_INTEGRATION_SERVICE_HOME" ]; then
      error "DATA_INTEGRATION_SERVICE_HOME environment variable is required. See --help for more information."
      exit 1
  fi

  if [ -z "$VERIFIER_HOME" ]; then
      error "VERIFIER_HOME environment variable is required. See --help for more information."
      exit 1
  fi

  if [ -z "$EVOTING_INFRASTRUCTURE_HOME" ]; then
      error "EVOTING_INFRASTRUCTURE_HOME environment variable is required. See --help for more information."
      exit 1
  fi
}

# Function to prompt the user for which dataset to use
prompt_for_dataset() {
    info "Multiple datasets are available to perform the end-to-end test:\n\t[1] Post_E2E_DEV\n\t[2] Post_E2E_DEV_1000\n\t[3] Post_E2E_DEV_10000\n\t[4] Post_E2E_DEV_1000_primary_secondary"
    read -p "Which dataset do you want to use? [1/2/3/4] (empty answer falls back on choice 1): " response
    case "$response" in
        2)
            DATASET='Post_E2E_DEV_1000'
            ;;
        3)
            DATASET='Post_E2E_DEV_10000'
            ;;
        4)
            DATASET='Post_E2E_DEV_primary_secondary'
            ;;
        *)
            ;;
    esac

    info "Dataset $DATASET selected."
}

############
### Main ###
############
if [ -z "$1" ]; then
  if [ -z "$EVOTING_COMPUTERS_HOME" ]; then
    error "EVOTING_COMPUTERS_HOME environment variable is required since --path option has not been provided. See --help for more information."
    exit 1
  fi
  warn "No --path provided. Using the EVOTING_COMPUTERS_HOME environment variable as target directory."
  DIR=$EVOTING_COMPUTERS_HOME
fi
while [[ $# -gt 0 ]]; do
  case $1 in
    --path)
      DIR=$(cygpath.exe --mixed -a "$2")
      shift
      shift
      ;;
    -h|--help)
      echo "Usage: prepare-computers-e2e.sh [OPTIONS]"
      echo "Setup the different computers needed for an end to end test."
      echo "Pre-requisites:"
      echo -e "- The EVOTING_HOME environment variable must be set."
      echo -e "- The DATA_INTEGRATION_SERVICE_HOME environment variable must be set."
      echo -e "- The VERIFIER_HOME environment variable must be set."
      echo -e "- The EVOTING_INFRASTRUCTURE_HOME environment variable must be set."
      echo -e "- The EVOTING_COMPUTERS_HOME environment variable can optionally be set and is required in case the --path option is not provided."
      echo
      echo "Options:"
      echo -e '--path [DIRECTORY] \t Provide the path to the target directory where the PCs will be created.'
      echo
      exit 0
      ;;
    *)
      info "Unknown option $1"
      exit 1
      ;;
  esac
done

check_mandatory_options

# Prepare target directory (exist or create, is empty)
if [ -d "$DIR" ]
then
	if [ "$(ls -A "$DIR")" ]; then
     prompt_for_deletion
	else
    info "Directory exists and is empty."
	fi
else
  mkdir -p "$DIR"
	info "Directory $DIR created."
fi

prompt_for_dataset

info "Preparing computers to perform an end-to-end. Please wait..."

${EVOTING_INFRASTRUCTURE_HOME}/scripts/prepare-pc-online.sh \
--path "${DIR}" \
--debug-config "${EVOTING_INFRASTRUCTURE_HOME}/testdata/secure-data-manager-online/electron/debug-config.json" \
--secure-data-manager "$(cygpath.exe --mixed -a $EVOTING_HOME)/secure-data-manager/secure-data-manager-packaging/target/secure-data-manager-package-*.zip" \
--seed "_EE_$(grep -A1 '"key": "sdm\.election\.event\.seed"' "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-online/electron/settings.json" | grep -o '"value": "[^"]*' | grep -o '[^"]*$')" \
--settings "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-online/electron/settings.json" \
--stop-docker \
--create-shortcuts \
--skip-delete-unzip-sdm

${EVOTING_INFRASTRUCTURE_HOME}/scripts/prepare-pc-setup.sh \
--path "${DIR}" \
--debug-config "${EVOTING_INFRASTRUCTURE_HOME}/testdata/secure-data-manager-setup/electron/debug-config.json" \
--secure-data-manager "$(cygpath.exe --mixed -a $EVOTING_HOME)/secure-data-manager/secure-data-manager-packaging/target/secure-data-manager-package-*.zip" \
--data-integration-service "$(cygpath.exe --mixed -a $DATA_INTEGRATION_SERVICE_HOME)/target/data-integration-service-*.zip" \
--seed "_EE_$(grep -A1 '"key": "sdm\.election\.event\.seed"' "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-setup/electron/settings.json" | grep -o '"value": "[^"]*' | grep -o '[^"]*$')" \
--settings "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-setup/electron/settings.json" \
--secure-data-manager-direct-trust-keystore "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-setup/direct-trust/local_direct_trust_keystore_sdm_config.p12" \
--secure-data-manager-direct-trust-password "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-setup/direct-trust/local_direct_trust_pw_sdm_config.txt" \
--data-integration-service-direct-trust-keystore "$EVOTING_INFRASTRUCTURE_HOME/testdata/canton/direct-trust/local_direct_trust_keystore_canton.p12" \
--data-integration-service-direct-trust-password "$EVOTING_INFRASTRUCTURE_HOME/testdata/canton/direct-trust/local_direct_trust_pw_canton.txt" \
--copy-dis-data "$EVOTING_INFRASTRUCTURE_HOME/testdata/tools/DIS/$DATASET" \
--stop-docker \
--create-shortcuts \
--skip-unzip-sdm \
--skip-delete-unzip-sdm

${EVOTING_INFRASTRUCTURE_HOME}/scripts/prepare-pc-tally.sh \
--path "${DIR}" \
--debug-config "${EVOTING_INFRASTRUCTURE_HOME}/testdata/secure-data-manager-tally/electron/debug-config.json" \
--secure-data-manager "$(cygpath.exe --mixed -a $EVOTING_HOME)/secure-data-manager/secure-data-manager-packaging/target/secure-data-manager-package-*.zip" \
--seed "_EE_$(grep -A1 '"key": "sdm\.election\.event\.seed"' "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-tally/electron/settings.json" | grep -o '"value": "[^"]*' | grep -o '[^"]*$')" \
--settings "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-tally/electron/settings.json" \
--direct-trust-keystore "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-tally/direct-trust/local_direct_trust_keystore_sdm_tally.p12" \
--direct-trust-password "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-tally/direct-trust/local_direct_trust_pw_sdm_tally.txt" \
--stop-docker \
--create-shortcuts \
--skip-unzip-sdm

${EVOTING_INFRASTRUCTURE_HOME}/scripts/prepare-pc-verifier.sh \
--path "${DIR}" \
--verifier "$(cygpath.exe --mixed -a "$VERIFIER_HOME")/verifier-assembly/target/verifier-assembly-*.zip" \
--decryption-password "$(grep -A1 '"key": "verifier\.export\.zip.password"' "$EVOTING_INFRASTRUCTURE_HOME/testdata/secure-data-manager-tally/electron/settings.json" | grep -o '"value": "[^"]*' | grep -o '[^"]*$')" \
--direct-trust-keystore "$EVOTING_INFRASTRUCTURE_HOME/testdata/verifier/direct-trust/local_direct_trust_keystore_verifier.p12" \
--direct-trust-password "$EVOTING_INFRASTRUCTURE_HOME/testdata/verifier/direct-trust/local_direct_trust_pw_verifier.txt" \
--create-shortcuts

info "Computers are now ready to perform an end-to-end test."

open_explorer
