#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#
#

# Exit if any line failed
set -e

# Global variables.
PC_NAME="PC-SETUP"

# Secure Data Manager folders.
DIRECT_TRUST_SETUP="direct-trust-setup"
PRINT_OUTPUT="Print-Output"
SDM="secure-data-manager"
SDM_WORKSPACE="SDM-workspace"
SDM_OUTPUT="SDM-Output"
VERIFIER_OUTPUT="VERIFIER-Output"

# Data Integration Service folders.
DIRECT_TRUST_CANTON="direct-trust-canton"
DATA_INTEGRATION_SERVICE="data-integration-service"
DIS_INPUT="DIS-input"
DIS_OUTPUT="DIS-output"

DEBUG_CONFIG=''
DIR=''
SECURE_DATA_MANAGER_PACKAGE=''
DATA_INTEGRATION_SERVICE_PACKAGE=''
SEED=''
SETTINGS_JSON=''
SDM_DIRECT_TRUST_KEYSTORE=''
SDM_DIRECT_TRUST_PASSWORD=''
DIS_DIRECT_TRUST_KEYSTORE=''
DIS_DIRECT_TRUST_PASSWORD=''
DIS_ELECTION_DATA_FOLDER=''
STOP_DOCKER=false
CREATE_SHORTCUTS=false
SKIP_UNZIP_SDM=false
SKIP_DELETE_UNZIP_SDM=false

source "${EVOTING_INFRASTRUCTURE_HOME}"/scripts/utils.sh

check_mandatory_options() {
  if [ -z "$DEBUG_CONFIG" ]; then
      error "--debug-config [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$DIR" ]; then
      error "--path [DIRECTORY] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SECURE_DATA_MANAGER_PACKAGE" ]; then
      error "--secure-data-manager [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SEED" ]; then
      error "--seed [STRING] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SETTINGS_JSON" ]; then
      error "--settings [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SDM_DIRECT_TRUST_KEYSTORE" ]; then
      error "--direct-trust-keystore [FILE] option is expected. See --help for more information."
      exit 1
  fi

  if [ -z "$SDM_DIRECT_TRUST_PASSWORD" ]; then
      error "--direct-trust-password [FILE] option is expected. See --help for more information."
      exit 1
  fi

    if [ -z "$DIS_ELECTION_DATA_FOLDER" ]; then
        error "--copy-dis-data [DIRECTORY] option is expected. See --help for more information."
        exit 1
    fi
}

create_secure_data_manager_shortcut() {
  if [ "$CREATE_SHORTCUTS" = true ] ; then
    # Check if running on Windows
    if [[ "$(uname -s)" == "MINGW"* ]]; then
      # Check if running with administrative privileges
      if net session > /dev/null 2>&1; then
        info "Creating Secure Data Manager shortcut..."

        # Create the symbolic link
        SYMBOLIC_LINK="$DIR/$PC_NAME - Secure Data Manager"

        # Create the symbolic link using PowerShell
        powershell -Command "New-Item -ItemType SymbolicLink -Path \"$SYMBOLIC_LINK\" -Target \"$PC_SETUP_SDM/SecureDataManager.exe\"" > /dev/null 2>&1;

        info "You can now run $SYMBOLIC_LINK."
        return
      else
        warn "This script does not seem to be running with administrative privileges. Shortcut will NOT be created."
      fi
    else
      warn "This script does not seem to be running on Windows. Shortcut will NOT be created."
    fi
  fi
  info "You can now run $PC_SETUP_SDM/SecureDataManager.exe."
}

prepare_secure_data_manager() {
  # Unzip sdm
  DIR_SDM="$DIR/$SDM"
  if [ "$SKIP_UNZIP_SDM" = false ] ; then
    # Validate the Secure Data Manager package is present
    if compgen -G "$SECURE_DATA_MANAGER_PACKAGE" > /dev/null; then
      info "Secure Data Manager package found with version $(basename $SECURE_DATA_MANAGER_PACKAGE | sed 's/secure-data-manager-package-\(.*\)\.zip/\1/')."
    else
      warn "Impossible to find the packaged Secure Data Manager. Please run the Secure Data Manager packaging to be able to install the Secure Data Manager in ${PC_NAME}. Ignoring."
      exit 0
    fi

    mkdir "$DIR_SDM"
    info "Starting extraction of Secure Data Manager. This may take a while."
    unzip -o "$SECURE_DATA_MANAGER_PACKAGE" -d "$DIR_SDM" > /dev/null
  fi

  info "Configuring Secure Data Manager..."

  PC_SETUP_SDM_WORKSPACE="$PC_SETUP_SEED/$SDM_WORKSPACE"
  mkdir -p "$PC_SETUP_SDM_WORKSPACE"

  PC_SETUP_SDM_OUTPUT="$PC_SETUP_SEED/$SDM_OUTPUT"
  mkdir -p "$PC_SETUP_SDM_OUTPUT"

  PC_SETUP_PRINT_OUTPUT="$PC_SETUP_SEED/$PRINT_OUTPUT"
  mkdir -p "$PC_SETUP_PRINT_OUTPUT"

  PC_SETUP_VERIFIER_OUTPUT="$PC_SETUP_SEED/$VERIFIER_OUTPUT"
  mkdir -p "$PC_SETUP_VERIFIER_OUTPUT"

  PC_SETUP_DIRECT_TRUST_SETUP="$PC_SETUP/$DIRECT_TRUST_SETUP"
  mkdir -p "$PC_SETUP_DIRECT_TRUST_SETUP"

  cp "$SDM_DIRECT_TRUST_KEYSTORE" "$PC_SETUP_DIRECT_TRUST_SETUP"
  cp "$SDM_DIRECT_TRUST_PASSWORD" "$PC_SETUP_DIRECT_TRUST_SETUP"

  PC_SETUP_SDM="$PC_SETUP/$SDM"
  cp -R "$DIR_SDM" "$PC_SETUP"

  PC_SETUP_DIS_OUTPUT="$PC_SETUP_SEED/$DIS_OUTPUT"
  mkdir -p "$PC_SETUP_DIS_OUTPUT"
  cp -R "$DIS_ELECTION_DATA_FOLDER/output/." "$PC_SETUP_DIS_OUTPUT"

  cp "$DEBUG_CONFIG" "$PC_SETUP_SDM/resources/app/electron"
  cp "$SETTINGS_JSON" "$PC_SETUP_SDM/resources/app/electron"

  PC_SETUP_SETTINGS_FILE="$PC_SETUP_SDM/resources/app/electron/settings.json"
  sed -i "s#\"value\": \"OUTPUT_PATH_TO_REPLACE\"#\"value\": \"$PC_SETUP_SDM_OUTPUT\"#" "$PC_SETUP_SETTINGS_FILE"
  sed -i "s#\"value\": \"EXTERNAL_CONFIGURATION_PATH_TO_REPLACE\"#\"value\": \"$PC_SETUP_DIS_OUTPUT\"#" "$PC_SETUP_SETTINGS_FILE"
  sed -i "s#\"value\": \"OUTPUT_PRINT_PATH_TO_REPLACE\"#\"value\": \"$PC_SETUP_PRINT_OUTPUT\"#" "$PC_SETUP_SETTINGS_FILE"
  sed -i "s#\"value\": \"OUTPUT_VERIFIER_PATH_TO_REPLACE\"#\"value\": \"$PC_SETUP_VERIFIER_OUTPUT\"#" "$PC_SETUP_SETTINGS_FILE"
  sed -i "s#\"value\": \"SETUP_WORKSPACE_TO_REPLACE\"#\"value\": \"$PC_SETUP_SDM_WORKSPACE\"#" "$PC_SETUP_SETTINGS_FILE"
  PC_SETUP_DIRECT_TRUST_SETUP_PATH="$PC_SETUP_DIRECT_TRUST_SETUP/local_direct_trust_keystore_sdm_config.p12"
  PC_SETUP_DIRECT_TRUST_PWD_SETUP_PATH="$PC_SETUP_DIRECT_TRUST_SETUP/local_direct_trust_pw_sdm_config.txt"
  sed -i "s#\"value\": \"DIRECT_TRUST_SETUP_PATH_TO_REPLACE\"#\"value\": \"$PC_SETUP_DIRECT_TRUST_SETUP_PATH\"#" "$PC_SETUP_SETTINGS_FILE"
  sed -i "s#\"value\": \"DIRECT_TRUST_PWD_SETUP_PATH_TO_REPLACE\"#\"value\": \"$PC_SETUP_DIRECT_TRUST_PWD_SETUP_PATH\"#" "$PC_SETUP_SETTINGS_FILE"

  if [ "$SKIP_DELETE_UNZIP_SDM" = false ] ; then
    info "Removing originally extracted Secure Data Manager package..."
    rm -rf "$DIR_SDM"
  fi

  if [ "$STOP_DOCKER" = true ] ; then
    # Stop Secure Data Manager dockerized instance if exist.
    info "Stopping Docker Secure Data Manager setup container..."
    docker stop secure-data-manager-setup > /dev/null 2>&1 || true
  fi

  create_secure_data_manager_shortcut
}

create_data_integration_service_shortcut() {
  if [ "$CREATE_SHORTCUTS" = true ] ; then
    # Check if running on Windows
    if [[ "$(uname -s)" == "MINGW"* ]]; then
      # Check if running with administrative privileges
      if net session > /dev/null 2>&1; then
        info "Creating Data Integration Service shortcut..."
        SYMBOLIC_LINK="$DIR/$PC_NAME - Data Integration Service"

        # Create the symbolic link using PowerShell
        powershell -Command "New-Item -ItemType SymbolicLink -Path \"$SYMBOLIC_LINK\" -Target \"$PC_SETUP_DATA_INTEGRATION_SERVICE/$RUN_DATA_INTEGRATION_SERVICE_BAT\"" > /dev/null 2>&1;

        info "You can now run $SYMBOLIC_LINK."
        return
      else
        warn "This script does not seem to be running with administrative privileges. Shortcut will NOT be created."
      fi
    else
      warn "This script does not seem to be running on Windows. Shortcut will NOT be created."
    fi
  fi
  info "You can now run $PC_SETUP_DATA_INTEGRATION_SERVICE/$RUN_DATA_INTEGRATION_SERVICE_BAT file."
}

prepare_data_integration_service() {
  # Validate the Data Integration Service assembly is present
  if compgen -G "$DATA_INTEGRATION_SERVICE_PACKAGE" > /dev/null; then
    info "Data Integration Service assembly found with version $(basename $DATA_INTEGRATION_SERVICE_PACKAGE | sed 's/data-integration-service-\(.*\)\.zip/\1/')."
  else
    warn "Impossible to find the assembled Data Integration Service. Please run the Data Integration Service assembly to be able to install the Data Integration Service in ${PC_NAME}. Ignoring."
    exit 0
  fi

  # Unzip Data Integration Service
  DIR_DIS="$DIR/$DATA_INTEGRATION_SERVICE"
  mkdir "$DIR_DIS"

  info "Starting extraction of Data Integration Service. This may take a while."
  unzip -o "$DATA_INTEGRATION_SERVICE_PACKAGE" -d "$DIR_DIS" > /dev/null

  info "Configuring Data Integration Service..."

  PC_SETUP_DIS_INPUT="$PC_SETUP/$SEED/$DIS_INPUT"
  mkdir -p "$PC_SETUP_DIS_INPUT"
  cp -R "$DIS_ELECTION_DATA_FOLDER/input/." "$PC_SETUP_DIS_INPUT"

  PC_SETUP_DIRECT_TRUST_CANTON="$PC_SETUP/$DIRECT_TRUST_CANTON"
  mkdir -p "$PC_SETUP_DIRECT_TRUST_CANTON"

  cp "$DIS_DIRECT_TRUST_KEYSTORE" "$PC_SETUP_DIRECT_TRUST_CANTON"
  cp "$DIS_DIRECT_TRUST_PASSWORD" "$PC_SETUP_DIRECT_TRUST_CANTON"

  PC_SETUP_DATA_INTEGRATION_SERVICE="$PC_SETUP/$DATA_INTEGRATION_SERVICE"
  cp -R "$DIR_DIS" "$PC_SETUP"

  PC_SETUP_DIRECT_TRUST_CANTON_PATH="$PC_SETUP_DIRECT_TRUST_CANTON/local_direct_trust_keystore_canton.p12"
  PC_SETUP_DIRECT_TRUST_CANTON_PWD_PATH="$PC_SETUP_DIRECT_TRUST_CANTON/local_direct_trust_pw_canton.txt"

  {
    echo "direct.trust.keystore.location.canton=$PC_SETUP_DIRECT_TRUST_CANTON_PATH"
    echo "direct.trust.keystore.password.location.canton=$PC_SETUP_DIRECT_TRUST_CANTON_PWD_PATH"
    echo "directory.source=$PC_SETUP_DIS_INPUT"
    echo "directory.target=$PC_SETUP_DIS_OUTPUT"
    echo "bypass.checkConsistency.dates=true"
  } >> "$PC_SETUP_DATA_INTEGRATION_SERVICE"/application.properties

  RUN_DATA_INTEGRATION_SERVICE_BAT="run-data-integration-service.bat"

  {
    echo "$PC_SETUP_DATA_INTEGRATION_SERVICE/embedded-jre/bin/java.exe -jar $(ls $PC_SETUP_DATA_INTEGRATION_SERVICE/data-integration-service-*.jar) --spring.config.location=$PC_SETUP_DATA_INTEGRATION_SERVICE/application.properties"
  } >> "$PC_SETUP_DATA_INTEGRATION_SERVICE/$RUN_DATA_INTEGRATION_SERVICE_BAT"

  info "Removing originally extracted Data Integration Service assembly..."
  rm -rf "$DIR_DIS"

  create_data_integration_service_shortcut
}

############
### Main ###
############

while [[ $# -gt 0 ]]; do
  case $1 in
    --debug-config)
      DEBUG_CONFIG="$2"
      shift
      shift
      ;;
    --path)
      DIR=$(cygpath.exe --mixed -a "$2")
      shift
      shift
      ;;
    --secure-data-manager)
      SECURE_DATA_MANAGER_PACKAGE="$2"
      shift
      shift
      ;;
    --data-integration-service)
      DATA_INTEGRATION_SERVICE_PACKAGE="$2"
      shift
      shift
      ;;
    --seed)
      SEED="$2"
      shift
      shift
      ;;
    --settings)
      SETTINGS_JSON="$2"
      shift
      shift
      ;;
    --secure-data-manager-direct-trust-keystore)
      SDM_DIRECT_TRUST_KEYSTORE="$2"
      shift
      shift
      ;;
    --secure-data-manager-direct-trust-password)
      SDM_DIRECT_TRUST_PASSWORD="$2"
      shift
      shift
      ;;
    --data-integration-service-direct-trust-keystore)
      DIS_DIRECT_TRUST_KEYSTORE="$2"
      shift
      shift
      ;;
    --data-integration-service-direct-trust-password)
      DIS_DIRECT_TRUST_PASSWORD="$2"
      shift
      shift
      ;;
    --stop-docker)
      STOP_DOCKER=true
      shift
      ;;
    --copy-dis-data)
      DIS_ELECTION_DATA_FOLDER="$2"
      shift
      shift
      ;;
    --create-shortcuts)
      CREATE_SHORTCUTS=true
      shift
      ;;
    --skip-unzip-sdm)
      SKIP_UNZIP_SDM=true
      shift
      ;;
    --skip-delete-unzip-sdm)
      SKIP_DELETE_UNZIP_SDM=true
      shift
      ;;
    -h|--help)
      echo "Usage: prepare-pc-setup.sh [OPTIONS]"
      echo "Prepare the setup PC. The script will initiate the needed folders for executing:"
      echo -e '\t - the Secure Data Manager'
      echo -e '\t - the Data Integration Service'
      echo
      echo "Options:"
      echo -e '--debug-config [FILE] \t\t\t\t\t Provide the path to the debug configuration file to be used in the Secure Data Manager.'
      echo -e '--path [DIRECTORY] \t\t\t\t\t Provide the path to the target directory where the PC will be created.'
      echo -e '--secure-data-manager [FILE] \t\t\t\t Provide the path to the Secure Data Manager package file to be installed.'
      echo -e '--data-integration-service [FILE] \t\t\t Provide the path to the Data Integration Service package file to be installed.'
      echo -e '--seed [STRING] \t\t\t\t\t Provide the seed string.'
      echo -e '--settings [FILE] \t\t\t\t\t Provide the path to the settings file to be used in the Secure Data Manager.'
      echo -e '--secure-data-manager-direct-trust-keystore [FILE] \t Provide the path to the direct trust keystore file to be used in the Secure Data Manager.'
      echo -e '--secure-data-manager-direct-trust-password [FILE] \t Provide the path to the direct trust password file to be used in the Secure Data Manager.'
      echo -e '--data-integration-service-direct-trust-keystore [FILE]\t Provide the path to the direct trust keystore file to be used in the Data Integration Service.'
      echo -e '--data-integration-service-direct-trust-password [FILE]\t Provide the path to the direct trust password file to be used in the Data Integration Service.'
      echo -e '--copy-dis-data [DIRECTORY] \t\t\t\t Provide the path to the DIS election event directory to be copied to the Data Integration Service input/output directories.'
      echo -e '--stop-docker \t\t\t\t\t\t Optional: stop the Docker Secure Data Manager setup container.'
      echo -e '--create-shortcuts \t\t\t\t\t Optional: create shortcuts for the applications in provided path directory.'
      echo -e '--skip-unzip-sdm \t\t\t\t\t Optional: skip the unzip of the Secure Data Manager package.'
      echo -e '--skip-delete-unzip-sdm \t\t\t\t Optional: skip the deletion of the unzipped Secure Data Manager package.'
      echo
      exit 0
      ;;
    *)
      error "Unknown option $1"
      exit 1
      ;;
  esac
done

check_mandatory_options

PC_SETUP="$DIR/$PC_NAME"

# Prepare target directory (exist or create, is empty)
if [ -d "$DIR" ]
then
	if [ -d "$PC_SETUP" ]; then
     error "$PC_SETUP already exists. Consider removing it before retrying. Exiting."
     exit 1
	fi
else
  mkdir -p "$DIR"
	info "Directory $DIR created."
fi

info "Creating $PC_SETUP..."
mkdir "$PC_SETUP"

PC_SETUP_SEED="$PC_SETUP/$SEED"
mkdir "$PC_SETUP_SEED"

prepare_secure_data_manager
prepare_data_integration_service
