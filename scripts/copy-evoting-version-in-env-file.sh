#!/usr/bin/env bash
#
# Copyright 2024 by Swiss Post, Information Technology Services
#
#

set -o errexit

OPTIONAL_SETTINGS="$*"

getEvotingVersion3Digits() {
  local evotingVersion4Digits=$1
  local regex="^([0-9]+\.[0-9]+\.[0-9]+)"

  if [[ $evotingVersion4Digits =~ $regex ]]; then
    echo "${BASH_REMATCH[1]}"
  fi
}

set_evoting_version() {
  export PROJECT_VERSION=$(grep -m 1 '<version>' ${EVOTING_HOME}/pom.xml | sed 's/.*<version>\(.*\)<\/version>.*/\1/')
  sed -i -r "s/^(PROJECT_VERSION=).*/\1${PROJECT_VERSION}/" "$EVOTING_INFRASTRUCTURE_HOME/docker-compose/.env"
  export EVOTING_VERSION=$(getEvotingVersion3Digits "$PROJECT_VERSION")
  sed -i -r "s/^(EVOTING_VERSION=).*/\1${EVOTING_VERSION}/" "$EVOTING_INFRASTRUCTURE_HOME/docker-compose/.env"

}
set_evoting_version
