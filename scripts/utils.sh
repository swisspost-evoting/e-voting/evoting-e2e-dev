#!/bin/bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#
#

# Define color codes
RED='\033[0;31m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
GREY='\033[0;90m'
RESET='\033[0m'  # Reset text formatting

info() {
    echo -e "${GREY}$(date +"%T")${RESET} [${BLUE}INFO${RESET}] $1"
}

warn() {
    echo -e "${GREY}$(date +"%T")${RESET} [${YELLOW}WARN${RESET}] $1${RESET}"
}

error() {
    echo -e "${RED}$(date +"%T") [ERROR] $1${RESET}"
}

open_explorer() {
  (CURRENT_DIR=$(pwd)
  cd "${DIR}" || exit 1
  explorer .
  cd "${CURRENT_DIR}")  || exit 1 &
}

# Function to prompt the user for deletion
prompt_for_deletion() {
    read -p "$DIR already exists and is not empty. Do you want to delete it? [y/n]: " response
    case "$response" in
        [yY]|[yY][eE][sS])
            info "Deleting content..."
            rm -rf "$DIR"
            ;;
        *)
            error "Exiting without deleting. Consider deleting the content manually it before retrying."
            exit 1
            ;;
    esac
}

reset_test_data() {
  rm -rf "${EVOTING_INFRASTRUCTURE_HOME}"/docker-compose/testdata
  cp -r "${EVOTING_INFRASTRUCTURE_HOME}"/testdata "${EVOTING_INFRASTRUCTURE_HOME}"/docker-compose/testdata
  mkdir -p "$EVOTING_INFRASTRUCTURE_HOME"/docker-compose/testdata/message-broker
  find "$EVOTING_HOME/message-broker/" -type d -name "node*" -exec cp -r {}  "$EVOTING_INFRASTRUCTURE_HOME"/docker-compose/testdata/message-broker \;
}
