FROM eclipse-temurin:21.0.4_7-jdk-jammy

USER root

ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"
ENV LANG en_US.UTF-8
ENV TIMEZONE='Europe/Zurich'

ARG DEBIAN_FRONTEND=noninteractive

RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked <<EOF

    set -exu

    # Update base OS
    apt-get -qq update
    apt-get -qq upgrade -y

    # Add temporary build alias
    alias install='apt-get -qq install -y --no-install-recommends'

    # Set timezone as downstream consumers expect it
    install tzdata \
            locales-all
    ln -fs /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
    dpkg-reconfigure -f noninteractive tzdata

    # Set baseuser up
    useradd baseuser

    # Install git
    install git

    # Install vim
    install vim

    # Install wine
    dpkg --add-architecture i386
    mkdir -pm755 /etc/apt/keyrings
    wget -O /etc/apt/keyrings/winehq-archive.key http://dl.winehq.org/wine-builds/winehq.key --no-check-certificate
    wget -NP /etc/apt/sources.list.d/ http://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources --no-check-certificate
    apt-get -o "Acquire::https::Verify-Peer=false" -qq update
    install -o "Acquire::https::Verify-Peer=false" winehq-stable

    # Install maven
    mkdir -p /opt/customtools/maven
    curl -ksSL -o maven.tar.gz https://archive.apache.org/dist/maven/maven-3/3.9.9/binaries/apache-maven-3.9.9-bin.tar.gz
    tar xvzf maven.tar.gz -C /opt/customtools/maven
    rm maven.tar.gz

    # Install node
    install xz-utils
    mkdir -p /opt/customtools/node
    curl -ksSL -o node.tar.xz https://nodejs.org/dist/v18.20.4/node-v18.20.4-linux-x64.tar.xz
    tar xvJf node.tar.xz -C /opt/customtools/node
    rm node.tar.xz

EOF

ENV MAVEN_HOME="/opt/customtools/maven/apache-maven-3.9.9/"
ENV NODE_HOME="/opt/customtools/node/node-v18.20.4-linux-x64/"
ENV PATH="${MAVEN_HOME}bin/:${NODE_HOME}bin:${PATH}"

USER baseuser
WORKDIR /home/baseuser
VOLUME [ "/home/baseuser/data" ]

COPY --chmod=755 resources/build.sh .
COPY --chmod=755 resources/environment-checker.sh .

ENTRYPOINT ["/bin/bash"]
