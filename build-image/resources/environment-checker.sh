#!/bin/bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#

while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      echo "Usage: environment-checker.sh"
      echo "Checks that the needed tools in expected version for a successful build are installed."
      exit 0
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

check_version() {
  if ! echo "$1" | egrep -q "$2"; then
    echo $3 "$4" >&2
    exit 1
  fi
}

# Define color codes
BLUE='\033[0;34m'
GREY='\033[0;90m'
RESET='\033[0m'  # Reset text formatting

info() {
    echo -e "${GREY}$(date +"%T")${RESET} [${BLUE}INFO${RESET}] $1"
}

info "Checking tools and versions. Please wait..."

check_version "$(java -version 2>&1)" 'version "21.0.4' 2 'java version'
check_version "$(mvn -v)" 'Apache Maven 3.9.9' 3 'maven version'
check_version "$(node -v)" 'v18.20.4' 3 'node version'
check_version "$(npm -v)" '10.7.0' 3 'npm version'

info "All needed tools in expected versions are installed."
