#!/usr/bin/env bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#

set -o errexit

evoting_version="master"
verifier_version="master"
data_integration_service_version="master"
fullexport="false"
repository_base_url="https://gitlab.com/swisspost-evoting/"
export_dir="/home/baseuser/data"

# Define color codes
BLUE='\033[0;34m'
GREY='\033[0;90m'
RESET='\033[0m'  # Reset text formatting

info() {
    echo -e "${GREY}$(date +"%T")${RESET} [${BLUE}INFO${RESET}] $1"
}

# Function to clean the workspace.
clean() {
  rm -rf e-voting
  rm -rf evoting-e2e-dev
  rm -rf crypto-primitives*
  rm -rf e-voting-libraries
  rm -rf verifier
  rm -rf data-integration-service
}

while [[ $# -gt 0 ]]; do
  case $1 in
    -ev|--evoting-version)
      evoting_version="$2"
      shift
      shift
      ;;
    -vv|--verifier-version)
      verifier_version="$2"
      shift
      shift
      ;;
    -dv|--data-integration-service-version)
      data_integration_service_version="$2"
      shift
      shift
      ;;
    -fe|--full-export)
      fullexport="true"
      shift
      ;;
    -ms|--maven-settings)
      mvn_settings="-s $2"
      shift
      shift
      ;;
    -h|--help)
      echo "Usage: build.sh [OPTIONS]"
      echo "Runs the e-voting build process and hashes computation. Exports the result in an archive in ${export_dir}."
      echo "Note that this script performs the build omitting the execution of tests (unit and integration) such that it can be done faster."
      echo
      echo -e '-ev, --evoting-version [VERSION] \t Provide a specific e-voting version to be cloned. Default is master branch as available on '${repository_base_url}e-voting/e-voting.
      echo -e '-vv, --verifier-version [VERSION] \t Provide a specific verifier version to be cloned. Default is master branch as available on '${repository_base_url}verifier/verifier.
      echo -e '-dv, --data-integration-service-version [VERSION] \t Provide a specific data-integration-service version to be cloned. Default is master branch as available on '${repository_base_url}e-voting/data-integration-service.
      echo -e '-fe, --full-export \t\t Activate full export in the final archive. Default content is e-voting, evoting-e2e-dev, verifier and hashes_256.txt.'
      echo -e '-ms, --maven-settings [PATH] \t Provide a custom maven settings file for the build process. Default is empty.'
      echo
      echo "WARN: This script is meant to be used in a Docker container running the evoting-build Docker image."
      exit 0
      ;;
    -*)
      info "Unknown option $1"
      exit 1
      ;;
  esac
done

./environment-checker.sh

# Clean workspace.
clean

# Turn off detached head advice.
git config --global advice.detachedHead false

info "Cloning repositories. Please wait..."

# Get into e-voting and get associated internal projects versions
if [ $evoting_version != "master" ]; then
  git clone -b e-voting-$evoting_version ${repository_base_url}e-voting/e-voting.git
else
  git clone -b $evoting_version ${repository_base_url}e-voting/e-voting.git
fi

crypto_primitives_version=$(grep -oP "(?<=<crypto-primitives.version\>).*(?=</crypto-primitives.version>)" e-voting/evoting-dependencies/pom.xml)
crypto_primitives_ts_version=$(grep -oP "(?<=<crypto-primitives-ts.version\>).*(?=</crypto-primitives-ts.version>)" e-voting/evoting-dependencies/pom.xml)
e_voting_libraries_version=$(grep -oP "(?<=<e-voting-libraries.version\>).*(?=</e-voting-libraries.version>)" e-voting/evoting-dependencies/pom.xml)
evoting_e2e_dev_version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout -f e-voting/pom.xml)

evoting_e2e_dev_tagInfo="$(git ls-remote --tags ${repository_base_url}e-voting/evoting-e2e-dev.git | grep infrastructure-$evoting_e2e_dev_version)" || true

# Clone each associated crypto-primitives libraries in associated versions

# crypto-primitives
git clone -b crypto-primitives-$crypto_primitives_version ${repository_base_url}crypto-primitives/crypto-primitives.git

# crypto-primitives-ts
git clone -b crypto-primitives-ts-$crypto_primitives_ts_version ${repository_base_url}crypto-primitives/crypto-primitives-ts.git

# Clone e-voting-libraries
git clone -b e-voting-libraries-$e_voting_libraries_version ${repository_base_url}e-voting/e-voting-libraries.git

# Clone verifier
git clone -b verifier-$verifier_version ${repository_base_url}verifier/verifier.git

# Clone data-integration-service
git clone -b data-integration-service-$data_integration_service_version ${repository_base_url}e-voting/data-integration-service.git

# Clean the package lock
find . \
  -type f -name 'package-lock.json' \
  -not -path "**/node_modules/**" \
  -not -path "**/target/**" \
  -not -path "**/dist/**" \
  -not -path "**/vendor/**" \
  -exec sed -i '/"resolved": "http.*/d' {} \;

# Build
# Maven options to clean install while skipping tests execution and ensuring parallel build with 1.5 threads per CPU where possible.
maven_options="clean install -DskipTests -T 1.5C $mvn_settings"
for module in "crypto-primitives" "crypto-primitives-ts" "e-voting-libraries" "e-voting" "verifier" "data-integration-service"; do
    info "Building $module. Please wait..."
    mvn $maven_options -f "$module"
    info "$module successfully built!"
done

hash_filename="hashes_256.txt"
info "Creating $hash_filename file. Please wait..."
find_file_selection=" -not -path "*/.m2/*" -not -path "*/archive-tmp/*" -a
(
-name "control-component-runnable*.jar"
-o -name "voting-server-*-runnable.jar"
-o -name "secure-data-manager-package*.zip"
-o -name "voter-portal-*.zip"
-o -name "direct-trust-tool-*.zip"
-o -name "file-cryptor-*-runnable.jar"
-o -name "xml-signature-*.jar"
-o -wholename "*/voter-portal/dist/crypto.ov-api.js"
-o -wholename "*/voter-portal/dist/crypto.ov-worker.js"
-o -wholename "*/voter-portal/dist/main.js"
-o -wholename "*/voter-portal/dist/polyfills.js"
-o -wholename "*/voter-portal/dist/runtime.js"
)"
true > $hash_filename
find e-voting \( $find_file_selection \) -exec sha256sum {} >> $hash_filename \;

find_file_selection=" -not -path "*/.m2/*" -not -path "*/archive-tmp/*" -a
(
-name "verifier-assembly-*.zip"
)"
find verifier \( $find_file_selection \) -exec sha256sum {} >> $hash_filename \;
find_file_selection=" -not -path "*/.m2/*" -not -path "*/archive-tmp/*" -a
(
-name "data-integration-service-*.zip"
)"
find data-integration-service \( $find_file_selection \) -exec sha256sum {} >> $hash_filename \;

export_default_folders="e-voting/ $hash_filename verifier/ data-integration-service/"

# Check if compatible end-to-end is available and clone it if so.
if [ -n "$evoting_e2e_dev_tagInfo" ]; then
  info "A compatible version of the end-to-end is available. Cloning, please wait..."
  git clone -b infrastructure-$evoting_e2e_dev_version ${repository_base_url}e-voting/evoting-e2e-dev.git
  export_default_folders="${export_default_folders} evoting-e2e-dev/"
  mv evoting-e2e-dev/scripts/e2e.sh ${export_dir}
else
  info "WARN: A compatible version of end-to-end is not available. Skipping the clone of the end-to-end."
fi

# Archive result of builds, computed hashes and (if available) end-to-end into a build.tar.gz file.
info "Creating the archive of builds and hashes into build.tar.gz. Please wait..."

if [ $fullexport == "true" ]; then
  tar czfh build.tar.gz $export_default_folders crypto-primitives/ crypto-primitives-ts/ e-voting-libraries/
else
  tar czfh build.tar.gz $export_default_folders
fi

info "Moving the archive build.tar.gz to ${export_dir}. Please wait..."

mv build.tar.gz ${export_dir}

info "Creation of the archive build.tar.gz complete! Please find it in ${export_dir}."
